﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GestionProduccionMuebles.Startup))]
namespace GestionProduccionMuebles
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
